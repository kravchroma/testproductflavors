package com.example.rom_pc.testproductflavors;

/**
 * Created by ROM_PC on 10.08.2016.
 */
public class Calculate {

    private int A;
    private int B;

    public Calculate(int a, int b) {
        A = a;
        B = b;
    }

    public int invoke() {
        return A + B + A + B;
    }
}
